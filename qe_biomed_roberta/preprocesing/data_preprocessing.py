import numpy as np
from transformers import AutoTokenizer, TFAutoModel, AutoModel
import tensorflow as tf

MAX_LENGTH = 128


class Data_PreProcessing(tf.keras.utils.Sequence):
    '''
    Data generator directly in batches
    '''

    def __init__(self, sentences, labels, batch_size, shuffle=True, include_targets=True):
        self.sentences = sentences
        self.labels = labels
        self.shuffle = shuffle
        self.batch_size = batch_size
        self.include_targets = include_targets
        AutoTokenizer.from_pretrained("allenai/biomed_roberta_base")
        self.indexes = np.arange(len(self.sentence_pairs))
        self.suffle()

    def __len__(self):
        return len(self.sentences) // self.batch_size

    def __getitem__(self, idx):
        # batch index compoutation
        indexes = self.indexes[idx * self.batch_size: (idx + 1) * self.batch_size]
        sentences = self.sentences[indexes]

        # tokenization
        tokenization = self.tokenizer.batch_encode_plus(
            sentences.tolist(),
            add_special_tokens=True,
            max_length=MAX_LENGTH,
            return_attention_mask=True,
            return_token_type_ids=True,
            pad_to_max_length=True,
            return_tensors="tf",
        )

        # input for the
        input_ids = np.array(tokenization["input_ids"], dtype="int32")
        attention_masks = np.array(tokenization["attention_mask"], dtype="int32")
        token_type_ids = np.array(tokenization["token_type_ids"], dtype="int32")

        if self.include_targets:
            labels = np.array(self.labels[indexes], dtype="int32")
            return [input_ids, attention_masks, token_type_ids], labels
        else:
            return [input_ids, attention_masks, token_type_ids]

    def suffle(self):
        # Shuffle indexes after each epoch if shuffle is set to True.
        if self.shuffle:
            np.random.RandomState(50).shuffle(self.indexes)
