import math
import pandas as pd
import tensorflow as tf


class Data_Loader:

    def __init__(self, path_to_train, path_to_validation, path_to_test, path_to_data, one_path=False):
        if one_path is False:
            self.train_data = pd.read_csv(path_to_train)
            self.validation_data = pd.read_csv(path_to_validation)
            self.test_data = pd.read_csv(path_to_test)
        else:
            self.dataset = pd.read_csv(path_to_data)
            total_size = len(self.dataset)
            train_size = math.floor(0.90 * total_size)

            self.train_data = self.dataset.head(train_size)
            self.validation_data = self.dataset.tail(len(self.dataset) - train_size)
            self.test_data = self.dataset.tail(len(self.dataset) - train_size)
            self.train_data.to_csv('train.csv')
            self.validation_data.to_csv('validation.csv')
            self.test_data.to_csv('test.csv')

    def get_data_features(self):
        print(f"Total train samples : {self.train_data.shape[0]}")
        print(f"Total validation samples: {self.validation_data.shape[0]}")
        print(f"Total test samples: {self.test_data.shape[0]}")

        print("Number of missing values")
        print(self.train_data.isnull().sum())
        self.train_data.dropna(axis=0, inplace=True)
        print("Train Target Distribution")
        print(self.train_data.similarity.value_counts())
        print("Validation Target Distribution")
        print(self.validation_data.similarity.value_counts())

    def get_data_overview(self):
        print(f"Premise: {self.train_data.loc[1, 'sentence1']}")
        print(f"Hypothesis: {self.train_data.loc[1, 'sentence2']}")
        print(f"Label: {self.train_data.loc[1, 'similarity']}")

    def prepare_train_data(self, SNLI, COV, SNLICOV):
        labels = ["contradiction", "entailment", "neutral"]
        if SNLI is True or SNLICOV is True:
            self.train_data["label"] = self.train_data["similarity"].apply(
                lambda x: 0 if x == labels[0] else 1 if x == labels[1] else 2)
            y_train = tf.keras.utils.to_categorical(self.train_data.label, num_classes=3)

        elif COV is True:
            self.train_data["label"] = self.train_data["gold_label"].apply(
                lambda x: 0 if x == labels[0] else 1 if x == labels[1] else 2)
            y_train = tf.keras.utils.to_categorical(self.train_data.label, num_classes=3)

        return self.train_data, y_train

    def prepare_validation_data(self, SNLI, COV, SNLICOV):
        labels = ["contradiction", "entailment", "neutral"]
        if SNLI is True or SNLICOV is True:
            self.validation_data["label"] = self.validation_data["similarity"].apply(
                lambda x: 0 if x == labels[0] else 1 if x == labels[1] else 2)
            y_val = tf.keras.utils.to_categorical(self.validation_data.label, num_classes=3)


        elif COV is True:
            self.validation_data["label"] = self.validation_data["gold_label"].apply(
                lambda x: 0 if x == labels[0] else 1 if x == labels[1] else 2)
            y_val = tf.keras.utils.to_categorical(self.validation_data.label, num_classes=3)

        return self.validation_data, y_val

    def prepare_test_data(self, SNLI, COV, SNLICOV, RQE):
        labels = ["contradiction", "entailment", "neutral"]
        if SNLI is True or SNLICOV is True:
            self.test_data["label"] = self.test_data["similarity"].apply(
                lambda x: 0 if x == labels[0] else 1 if x == labels[1] else 2
            )
            y_test = tf.keras.utils.to_categorical(self.test_data.label, num_classes=3)

        elif COV is True:
            self.test_data["label"] = self.test_data["gold_label"].apply(
                lambda x: 0 if x == labels[0] else 1 if x == labels[1] else 2
            )
            y_test = tf.keras.utils.to_categorical(self.test_data.label, num_classes=3)

        elif RQE is True:
            self.test_data["label"] = self.test_data["value"].apply(
                lambda x: 2 if x is False else 1 if x is True else 0
            )
            y_test = tf.keras.utils.to_categorical(self.test_data.label, num_classes=3)

        return self.test_data, y_test
