from tensorflow.python.keras import backend as K
import os
import tensorflow as tf
import matplotlib.pyplot as plt

from tensorflow.python.client import device_lib

from preprocesing.data_loading import Data_Loader
from preprocesing.data_preprocessing import Data_PreProcessing
from training.bert import BERTmodel

BERT_FLAVOUR = 'bert_base'
LEARNING_RATE = 1e-3
BATCH_SIZE = 16
EPOCHS = 10


def set_environment():
    print(device_lib.list_local_devices())
    print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = "0,1"


def data_processing(p_train, p_validation, p_test, p_data, one_path, SNLI, COV, SNLICOV, RQE):
    data_loader = Data_Loader(p_train, p_validation, p_test, p_data, one_path)
    data_loader.get_data_features()
    data_loader.get_data_overview()
    train, y_train = data_loader.prepare_train_data(SNLI, COV, SNLICOV)
    validation, y_val = data_loader.prepare_validation_data(SNLI, COV, SNLICOV)
    test, y_test = data_loader.prepare_test_data(SNLI, COV, SNLICOV, RQE)
    return train, y_train, validation, y_val, test, y_test


def load_model(load, path_to_weights):
    bert_model = BERTmodel()
    model = bert_model.create(BERT_FLAVOUR, learning_rate=LEARNING_RATE)
    if load is False:
        return model
    else:
        model.load_weights(path_to_weights)
        return model


def train(model, train, y_train, validation, y_val, test, y_test):
    train_data = Data_PreProcessing(
        train[["sentence1", "sentence2"]].values.astype("str"),
        y_train,
        batch_size=BATCH_SIZE,
        shuffle=True,
    )
    val_data = Data_PreProcessing(
        validation[["sentence1", "sentence2"]].values.astype("str"),
        y_val,
        batch_size=BATCH_SIZE,
        shuffle=False,

    )
    test_data = Data_PreProcessing(
        test[["sentence1", "sentence2"]].values.astype("str"),
        y_test,
        batch_size=BATCH_SIZE,
        shuffle=False,
    )

    print("Training:")

    tf_config = tf.compat.v1.ConfigProto(allow_soft_placement=False)
    tf_config.gpu_options.allow_growth = True
    s = tf.compat.v1.Session(config=tf_config)
    K.set_session(s)

    with tf.device('/gpu:0'):
        history = model.fit(
            train_data,
            validation_data=val_data,
            epochs=EPOCHS,
        )

    print("Finished training")

    print('Testing')

    model.evaluate(test_data, verbose=1)

    print("Finished testing")

    # plotting
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()

    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()


def save_model(model, path_to_dir, path_to_weights):
    if not os.path.exists(path_to_dir):
        os.makedirs(path_to_dir)

    print("Saving model to %s" % path_to_dir)
    model_to_save = model.module if hasattr(model, 'module') else model
    model_to_save.save(path_to_dir, save_format='tf')
    model_to_save.save_weights(path_to_weights, save_format='h5')


