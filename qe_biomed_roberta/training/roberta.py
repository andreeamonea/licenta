import numpy as np
import transformers
import tensorflow as tf


from preprocesing.data_preprocessing import Data_PreProcessing

max_length = 128  # Maximum length of input sentence to the model.
batch_size = 16
epochs = 2


class RoBERTamodel():

    def create(self, roberta_flavour, learning_rate):
        #  input layer for input ids
        input_ids = tf.keras.layers.Input(
            shape=(max_length,), dtype=tf.int32, name="input_ids"
        )
        # attention input layer
        attention_masks = tf.keras.layers.Input(
            shape=(max_length,), dtype=tf.int32, name="attention_masks"
        )
        # inout layer for masks
        token_type_ids = tf.keras.layers.Input(
            shape=(max_length,), dtype=tf.int32, name="token_type_ids"
        )

        # loading pretrained BERT model.
        bert_model = transformers.TFRobertaModel.from_pretrained(roberta_flavour)

        # freeze the model to reuse the pretrained features without modifying them
        bert_model.trainable = False

        # prepare the pre-trained model
        sequence_output, pooled_output = bert_model(
            input_ids, attention_mask=attention_masks, token_type_ids=token_type_ids
        )

        # extra architecture - layers on top of frozen layers
        # to adapt the pretrained features on the new data
        lstm1 = tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(64, return_sequences=True))(sequence_output)
        # pooling layer for reducing the output dimension
        avg_pool = tf.keras.layers.GlobalAveragePooling1D()(lstm1)
        max_pool = tf.keras.layers.GlobalMaxPooling1D()(lstm1)
        # concatenate pooling layers
        concat = tf.keras.layers.concatenate([avg_pool, max_pool])

        dropout = tf.keras.layers.Dropout(0.3)(concat)
        output = tf.keras.layers.Dense(3, activation="softmax")(dropout)

        # final model architecture
        model = tf.keras.models.Model(
            inputs=[input_ids, attention_masks, token_type_ids], outputs=output
        )

        # compile model with Adam learning rate given
        model.compile(
            optimizer=tf.keras.optimizers.Adam(learning_rate),
            loss="categorical_crossentropy",
            metrics=["acc"],
        )

        # print model architecture
        model.summary()

        return model


