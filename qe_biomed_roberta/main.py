from preprocesing.data_preprocessing import Data_PreProcessing
from training import train_bert
from training import train_roberta
from training import train_biobert
from testing import evaluate

if __name__ == '__main__':

   # bert experiments
   train_bert.set_environment()
   train, y_train, validation, y_val, test, y_test = train_bert.data_processing('datasets/SNLI_Corpus/snli_1.0_dev.csv',
                              'datasets/SNLI_Corpus/snli_1.0_dev.csv',
                              'datasets/SNLI_Corpus/snli_1.0_dev.csv', None, one_path = False,
                              SNLI = True,
                              COV = False,
                              SNLICOV= False,
                              RQE = False)
   bert_model = train_bert.load_model(False, None)
   train_bert.train(bert_model, train, y_train, validation, y_val, test, y_test)
   train_bert.save_model(bert_model, './models/bert_base_1e-3_10_16', './models/bert_base_1e-3_10_16.h5')

   train_bert.set_environment()
   train, y_train, validation, y_val, test, y_test = train_bert.data_processing(None, None, None,
                                                                                'datasets/CORD-19 NLI Dataset - csv.csv',
                                                                                one_path=True,
                                                                                SNLI=False,
                                                                                COV=True,
                                                                                SNLICOV=False,
                                                                                RQE=False)
   bert_model = train_bert.load_model(True, './models/bert_base_1e-3_10_16.h5')
   train_bert.train(bert_model, train, y_train, validation, y_val, test, y_test)
   train_bert.save_model(bert_model, './models/bert_snli_cov', './models/bert_snli_cov.h5')

   train_bert.set_environment()
   train, y_train, validation, y_val, test, y_test = train_bert.data_processing('datasets/train_snli_cov.csv',
                                                                                'datasets/validation_snli_cov.csv',
                                                                                'datasets/test_snli_cov.csv',
                                                                                None,
                                                                                one_path=False,
                                                                                SNLI=False,
                                                                                COV=False,
                                                                                SNLICOV=True,
                                                                                RQE=False)
   bert_model = train_bert.load_model(False, None)
   train_bert.train(bert_model, train, y_train, validation, y_val, test, y_test)
   train_bert.save_model(bert_model, './models/bert_snlicov', './models/bert_snlicov.h5')

   # roberta experiments
   train_roberta.set_environment()
   train, y_train, validation, y_val, test, y_test = train_roberta.data_processing('datasets/SNLI_Corpus/snli_1.0_dev.csv',
                                                                                'datasets/SNLI_Corpus/snli_1.0_dev.csv',
                                                                                'datasets/SNLI_Corpus/snli_1.0_dev.csv',
                                                                                None, one_path=False,
                                                                                SNLI=True,
                                                                                COV=False,
                                                                                SNLICOV=False,
                                                                                RQE=False)
   bert_model = train_roberta.load_model(False, None)
   train_roberta.train(bert_model, train, y_train, validation, y_val, test, y_test)
   train_roberta.save_model(bert_model, './models/bert_base_1e-3_10_16', './models/bert_base_1e-3_10_16.h5')

   train_roberta.set_environment()
   train, y_train, validation, y_val, test, y_test = train_roberta.data_processing(None, None, None,
                                                                                'datasets/CORD-19 NLI Dataset - csv.csv',
                                                                                one_path=True,
                                                                                SNLI=False,
                                                                                COV=True,
                                                                                SNLICOV=False,
                                                                                RQE=False)
   bert_model = train_roberta.load_model(True, './models/bert_base_1e-3_10_16.h5')
   train_roberta.train(bert_model, train, y_train, validation, y_val, test, y_test)
   train_roberta.save_model(bert_model, './models/bert_snli_cov', './models/bert_snli_cov.h5')

   train_bert.set_environment()
   train, y_train, validation, y_val, test, y_test = train_roberta.data_processing('datasets/train_snli_cov.csv',
                                                                                'datasets/validation_snli_cov.csv',
                                                                                'datasets/test_snli_cov.csv',
                                                                                None,
                                                                                one_path=False,
                                                                                SNLI=False,
                                                                                COV=False,
                                                                                SNLICOV=True,
                                                                                RQE=False)
   bert_model = train_roberta.load_model(False, None)
   train_roberta.train(bert_model, train, y_train, validation, y_val, test, y_test)
   train_roberta.save_model(bert_model, './models/roberta_dataset_snlicov', './models/roberta_dataset_snlicov.h5')

   # biobert experiments
   train_biobert.set_environment()
   train_biobert.set_environment()
   train, y_train, validation, y_val, test, y_test = train_biobert.data_processing('datasets/train_snli_cov.csv',
                                                                                   'datasets/validation_snli_cov.csv',
                                                                                   'datasets/test_snli_cov.csv',
                                                                                   None,
                                                                                   one_path=False,
                                                                                   SNLI=False,
                                                                                   COV=False,
                                                                                   SNLICOV=True,
                                                                                   RQE=False)
   bert_model = train_biobert.load_model(False, None)
   train_biobert.train(bert_model, train, y_train, validation, y_val, test, y_test)
   train_biobert.save_model(bert_model, './models/biobert_snli_cov', './models/biobert_snli_cov.h5')

   # test
   evaluate.set_environment()
   test_data, y_test = evaluate.data_processing(None, None, 'datasets/medqa_ent.csv', None, one_path=False, SNLI=False, COV=False,SNLICOV=False,
                                                                                RQE=True)
   model = evaluate.load_model(True, 'models/biobert_q.h5')
   evaluate.test_model(model, test_data, y_test)