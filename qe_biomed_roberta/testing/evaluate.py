from tensorflow.python.keras import backend as K
import os
import tensorflow as tf
from tensorflow.python.client import device_lib

from preprocesing.data_loading import Data_Loader
from preprocesing.data_preprocessing import Data_PreProcessing
from training.bert import BERTmodel
from training.biobert import BioMedRoBERTamodel

LEARNING_RATE = 1e-3
BATCH_SIZE = 16
EPOCHS = 5

def set_environment():
    print(device_lib.list_local_devices())
    print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = "0,1"


def data_processing(p_train, p_validation, p_test, p_data, one_path, SNLI, COV, SNLICOV, RQE):
    data_loader = Data_Loader(p_train, p_validation, p_test, p_data, one_path)
    # data_loader.get_data_features()
    # data_loader.get_data_overview()
    test, y_test = data_loader.prepare_test_data(SNLI, COV, SNLICOV, RQE)
    return test, y_test


def load_model(load, path_to_weights):
    bert_model = BioMedRoBERTamodel()
    model = bert_model.create(learning_rate=LEARNING_RATE)
    if load is False:
        return model
    else:
        model.load_weights(path_to_weights)
        return model


def test_model(model,  test, y_test):

    test_data = Data_PreProcessing(
        test[["chq", "faq"]].values.astype("str"),
        y_test,
        batch_size=BATCH_SIZE,
        shuffle=False,
    )

    print('Testing')

    model.evaluate(test_data, verbose=1)

    print("Finished testing")

