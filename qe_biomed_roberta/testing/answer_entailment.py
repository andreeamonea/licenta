import numpy as np
import pandas as pd

import csv


from preprocesing.data_preprocessing import Data_PreProcessing
from training import train_bert, train_roberta, train_biobert

ans1 = '''Kitty is a cat.'''
ans2 = '''Cats eat mice.'''

# model = create_model()
# model.load_weights("biobert_snli_cov.h5")
labels = ["entailment","contradiction", "neutral"]

df = pd.read_csv('medquad3.csv', header=None)
ds = df.sample(frac=1)
ds.to_csv('shuffle3.csv')

def return_contents(file_name):
    with open(file_name) as infile:
        reader = csv.reader(infile)
        return list(reader)

# data1 = return_contents('medquad3.csv')
# data2 = return_contents('shuffle3.csv')
combined = []
with open('./datasets/combined3.csv', 'w', newline='') as outfile:
   writer = csv.writer(outfile)
   columns = ['q1', 'ans1', 'qtype1', 'focus1', 'id2', 'q2', 'ans2', 'qtype2', 'focus2', 'label']
   writer.writerow(columns)
   for row1, row2 in zip(data1, data2):
        writer = csv.writer(outfile)
        combined = row1 + row2
        writer.writerow(combined)




def predict(model, sentence1, sentence2, f):
    sentence_pairs = np.array([[str(sentence1), str(sentence2)]])
    test_data = Data_PreProcessing(
        sentence_pairs, labels=None, batch_size=1, shuffle=False, include_targets=False,
    )


    proba = model.predict(test_data)[0]
    idx = np.argmax(proba)
    f.write(labels[0] + f"{proba[0]: .2f}%" + '\n')
    f.write(labels[1] + f"{proba[1]: .2f}%" + '\n')
    f.write(labels[2] + f"{proba[2]: .2f}%" + '\n')
    proba = f"{proba[idx]: .2f}%"
    pred = labels[idx]
    return pred, proba


# model = train_bert.load_model(False, None)
# model = train_roberta.load_model(False, None)
model = train_biobert.load_model(False, None)
print(predict(model, ans1, ans2))
from collections import defaultdict
columns = defaultdict(list)
with open('./datasets/medquad3.csv') as f:
    reader = csv.reader(f)
    list = []
    for row in reader:
        list.append(row)
    with open('entailment_q.csv', 'a') as ent_f:
        writer = csv.writer(ent_f)
        it = iter(list)
        for x in it:
            y = next(it)
            to_row = []
            to_row.append(x[0])
            to_row.append(y[0])
            if x[3] == y[3]:
                to_row.append('entailment')
            else:
                to_row.append('neutral')
            writer.writerow(to_row)
        ent_f.close()
